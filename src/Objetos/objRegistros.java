/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class objRegistros {
    private Integer cedula;
    private String nombre;
    private Integer edad;
    private String estadoJugador;
    private Integer equipo;
    private Integer destreza;
    private String fecha_debut;
    private Integer id_equipo;
    private String nombreEquipo;
    private String estadoEquipo;
    private Integer id_destreza;
    private String perfil;
    private String fortaleza;

    public objRegistros(Integer cedula, String nombre, Integer edad, String estadoJugador, Integer equipo, Integer destreza, String fecha_debut, Integer id_equipo, String nombreEquipo, String estadoEquipo, Integer id_destreza, String perfil, String fortaleza) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.edad = edad;
        this.estadoJugador = estadoJugador;
        this.equipo = equipo;
        this.destreza = destreza;
        this.fecha_debut = fecha_debut;
        this.id_equipo = id_equipo;
        this.nombreEquipo = nombreEquipo;
        this.estadoEquipo = estadoEquipo;
        this.id_destreza = id_destreza;
        this.perfil = perfil;
        this.fortaleza = fortaleza;
    }
    
    public static ArrayList listaRegistros=new ArrayList<>();

    /**
     * @return the cedula
     */
    public Integer getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public Integer getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    /**
     * @return the estadoJugador
     */
    public String getEstadoJugador() {
        return estadoJugador;
    }

    /**
     * @param estadoJugador the estadoJugador to set
     */
    public void setEstadoJugador(String estadoJugador) {
        this.estadoJugador = estadoJugador;
    }

    /**
     * @return the equipo
     */
    public Integer getEquipo() {
        return equipo;
    }

    /**
     * @param equipo the equipo to set
     */
    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    /**
     * @return the destreza
     */
    public Integer getDestreza() {
        return destreza;
    }

    /**
     * @param destreza the destreza to set
     */
    public void setDestreza(Integer destreza) {
        this.destreza = destreza;
    }

    /**
     * @return the fecha_debut
     */
    public String getFecha_debut() {
        return fecha_debut;
    }

    /**
     * @param fecha_debut the fecha_debut to set
     */
    public void setFecha_debut(String fecha_debut) {
        this.fecha_debut = fecha_debut;
    }

    /**
     * @return the id_equipo
     */
    public Integer getId_equipo() {
        return id_equipo;
    }

    /**
     * @param id_equipo the id_equipo to set
     */
    public void setId_equipo(Integer id_equipo) {
        this.id_equipo = id_equipo;
    }

    /**
     * @return the nombreEquipo
     */
    public String getNombreEquipo() {
        return nombreEquipo;
    }

    /**
     * @param nombreEquipo the nombreEquipo to set
     */
    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    /**
     * @return the estadoEquipo
     */
    public String getEstadoEquipo() {
        return estadoEquipo;
    }

    /**
     * @param estadoEquipo the estadoEquipo to set
     */
    public void setEstadoEquipo(String estadoEquipo) {
        this.estadoEquipo = estadoEquipo;
    }

    /**
     * @return the id_destreza
     */
    public Integer getId_destreza() {
        return id_destreza;
    }

    /**
     * @param id_destreza the id_destreza to set
     */
    public void setId_destreza(Integer id_destreza) {
        this.id_destreza = id_destreza;
    }

    /**
     * @return the perfil
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     * @return the fortaleza
     */
    public String getFortaleza() {
        return fortaleza;
    }

    /**
     * @param fortaleza the fortaleza to set
     */
    public void setFortaleza(String fortaleza) {
        this.fortaleza = fortaleza;
    }

       
}
