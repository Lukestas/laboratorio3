/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class objJugadores {

    private Integer cedula;
    private String nombre;
    private Integer edad;
    private String estado;
    private Integer equipo;
    private Integer destreza;
    private String fecha_debut;

    public objJugadores(Integer cedula, String nombre, Integer edad, String estado, Integer equipo, Integer destreza, String fecha_debut) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.edad = edad;
        this.estado = estado;
        this.equipo = equipo;
        this.destreza = destreza;
        this.fecha_debut = fecha_debut;
    }

    private static ArrayList listaJugadores = new ArrayList<>();

    /**
     * @return the cedula
     */
    public Integer getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public Integer getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the equipo
     */
    public Integer getEquipo() {
        return equipo;
    }

    /**
     * @param equipo the equipo to set
     */
    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    /**
     * @return the destreza
     */
    public Integer getDestreza() {
        return destreza;
    }

    /**
     * @param destreza the destreza to set
     */
    public void setDestreza(Integer destreza) {
        this.destreza = destreza;
    }

    /**
     * @return the fecha_debut
     */
    public String getFecha_debut() {
        return fecha_debut;
    }

    /**
     * @param fecha_debut the fecha_debut to set
     */
    public void setFecha_debut(String fecha_debut) {
        this.fecha_debut = fecha_debut;
    }

    /**
     * @return the listaJugadores
     */
    public static ArrayList getListaJugadores() {
        return listaJugadores;
    }

    /**
     * @param aListaJugadores the listaJugadores to set
     */
    public static void setListaJugadores(ArrayList aListaJugadores) {
        listaJugadores = aListaJugadores;
    }

}
