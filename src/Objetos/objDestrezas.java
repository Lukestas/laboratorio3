/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class objDestrezas {
    private String perfil;
    private String fortaleza;

    public objDestrezas(String perfil, String fortaleza) {
        this.perfil = perfil;
        this.fortaleza = fortaleza;
    }

    public static ArrayList listaDestrezas= new ArrayList<>();
    
    /**
     * @return the perfil
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     * @return the fortaleza
     */
    public String getFortaleza() {
        return fortaleza;
    }

    /**
     * @param fortaleza the fortaleza to set
     */
    public void setFortaleza(String fortaleza) {
        this.fortaleza = fortaleza;
    }
    
    
}
