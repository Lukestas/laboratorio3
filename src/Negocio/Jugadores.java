/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDFutbol;
import Objetos.objJugadores;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class Jugadores {
    BDFutbol dbFutbol = new BDFutbol();

    public void InsertarNuevoJugador(objJugadores jugador) {
        dbFutbol.insertarJugador(jugador);
    }
    
    public DefaultComboBoxModel cargarJugadores(){
        DefaultComboBoxModel modelo= new DefaultComboBoxModel();
        ArrayList<Integer> Jugadores= dbFutbol.cargarJugadores();
        modelo.addElement("Seleccione un Jugador");
        for (Integer nombres: Jugadores){
            modelo.addElement(nombres);
        }
        return modelo;
    }
    
    public objJugadores cargarUnicoJugador(Integer jugador){
        objJugadores datos=dbFutbol.cargarUnJugador(jugador);
        return datos;
    }
    
    public void modificarjugador(objJugadores jugador, Integer cedula){
        dbFutbol.modificarUnJugador(jugador,cedula);
    }
    
    public void EliminarJugador(Integer Cedula){
        dbFutbol.EliminacionDeJugador(Cedula);
    }
    
}
