/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDFutbol;
import Objetos.objEquipos;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class Equipos {

    BDFutbol dbFutbol = new BDFutbol();

    public void InsertarEquipoNuevo(objEquipos equipo) {
        dbFutbol.insertarEquipo(equipo);
    }

    public Integer ObtencionIdEquipo(String EquipoNombre) {
        return dbFutbol.obtenerIdEquipo(EquipoNombre);
    }
    
    public objEquipos cargarUnicoEquipo(Integer equipo){
        objEquipos datos=dbFutbol.cargarUnEquipo(equipo);
        return datos;
    }
    
    public void modificarEquipo(objEquipos equipo, Integer idEquipo){
        dbFutbol.modificarEquipo(equipo,idEquipo);
    }
    
    public void EliminarEquipo(Integer Equipo){
        dbFutbol.ElimacionDeEquipo(Equipo);
    }
    
    public DefaultComboBoxModel cargarEquipos(){
        DefaultComboBoxModel modelo= new DefaultComboBoxModel();
        ArrayList<String> Equipos= dbFutbol.cargarEquipos();
        modelo.addElement("Seleccione un Equipo");
        for (String nombres: Equipos){
            modelo.addElement(nombres);
        }
        return modelo;
    }
    
}
