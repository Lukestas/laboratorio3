/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDFutbol;
import Objetos.objRegistros;
import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class Registros {

    BDFutbol bdfutbol = new BDFutbol();

    public ArrayList<objRegistros> obtenerRespuestaParaFA(String Fortaleza) {
        ArrayList<objRegistros> regis = bdfutbol.cargarRegistrosActivosConFortalezas(Fortaleza);
        return regis;
    }
    
    public ArrayList<objRegistros> obtenerRespuestaParaEDAD(Integer Desde, Integer Hasta,String Fortaleza, Integer Equipo) {
        ArrayList<objRegistros> regis = bdfutbol.cargarRegistrosEdades(Desde, Hasta, Fortaleza, Equipo);
        return regis;
    }

    public ArrayList<objRegistros> obtenerRespuestaParaDB(String Fecha1, String Fecha2) {
        ArrayList<objRegistros> regis = bdfutbol.cargarRegistrosDebuts(Fecha1, Fecha2);
        return regis;
    }

    

}
