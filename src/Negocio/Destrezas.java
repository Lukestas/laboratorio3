/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDFutbol;
import Objetos.objDestrezas;

/**
 *
 * @author Lukestas
 */
public class Destrezas {
    BDFutbol bdfutbol= new BDFutbol();
    public Integer ObtencionDestrezaID(String perfil,String fortaleza){
        return bdfutbol.obtenerIdDestreza(perfil, fortaleza);
    }
    
    public objDestrezas ObtenerAtributosDestreza(Integer Destreza){
        objDestrezas datos= bdfutbol.cargarAtributosDestrezas(Destreza);
        return datos;
    }
    
}
