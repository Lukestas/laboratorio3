/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objetos.objDestrezas;
import Objetos.objEquipos;
import Objetos.objJugadores;
import Objetos.objRegistros;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Lukestas
 */
public class BDFutbol {

    private ResultSet rs = null;
    private Statement s = null;
    BDConexion conexion = new BDConexion();
    private Connection connection = null;

    public Integer cargarUltimoIDEquipo() {
        Integer i = -1;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select id_equipo from equipo order by id_equipo desc limit 1");
            while (rs.next()) {
                i = rs.getInt("id_equipo");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return i + 1;
    }

    public Integer obtenerIdEquipo(String NombreEquipo) {
        Integer i = -1;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select id_equipo from equipo where nombre_equipo='" + NombreEquipo + "'");
            while (rs.next()) {
                i = rs.getInt("id_equipo");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public Integer obtenerIdDestreza(String perfil, String Fortaleza) {
        Integer i = -1;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select id_destreza from destrezas where perfil='" + perfil + "' and fortaleza='" + Fortaleza + "'");
            while (rs.next()) {
                i = rs.getInt("id_destreza");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public ArrayList<String> cargarEquipos() {
        ArrayList<String> equipos = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select nombre_equipo from equipo");
            while (rs.next()) {
                equipos.add(rs.getString("nombre_equipo"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return equipos;
    }
    
    public ArrayList<Integer> cargarJugadores(){
        ArrayList<Integer> jugadores = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select cedula from jugadores");
            while (rs.next()) {
                jugadores.add(rs.getInt("cedula"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return jugadores;
    }

    public ArrayList<objRegistros> cargarRegistrosDebuts(String Fecha1, String Fecha2) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.nombre as nombre,e.nombre_equipo as nombre_equipo,j.fecha_debut as fecha_debut from jugadores j, equipo e where j.equipo=e.id_equipo and fecha_debut between '"+Fecha1+"' and '"+Fecha2+"'");
            while (rs.next()) {
                objRegistros.listaRegistros.add(new objRegistros(
                        -1, rs.getString("nombre"),
                        -1, "",
                        -1, -1,
                        rs.getString("fecha_debut"), -1,
                        rs.getString("nombre_equipo"), "",
                        -1, "",
                        ""));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "11Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return objRegistros.listaRegistros;
    }
    
    public ArrayList<objRegistros> cargarRegistrosEdades(Integer Desde, Integer Hasta, String Fortaleza, Integer Equipo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.cedula as cedula, "
                    + "j.nombre as nombre, "
                    + "j.edad as edad, "
                    + "j.fecha_debut as fecha_debut "
                    + "from jugadores j, destrezas d, equipo e "
                    + "where j.estado=true "
                    + "and j.equipo=e.id_equipo "
                    + "and j.equipo="+Equipo+""
                            + "and d.fortaleza='"+Fortaleza+"' "
                                    + "and j.destreza=d.id_destreza "
                                    + "and e.estado=true "
                                    + "and j.fecha_debut>='1997-01-01' "
                                    + "and edad between "+Desde+" and "+Hasta+" "
                                            + "order by j.nombre asc");
            while (rs.next()) {
                objRegistros.listaRegistros.add(new objRegistros(
                        rs.getInt("cedula"), rs.getString("nombre"),
                        rs.getInt("edad"), "",
                        -1, -1,
                        rs.getString("fecha_debut"), -1,
                        "", "",
                        -1, "",
                        ""));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return objRegistros.listaRegistros;
    }
    public objEquipos cargarUnEquipo(Integer Equipo){
        objEquipos datos=new objEquipos("","");
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select e.nombre_equipo as nombre, e.estado as estado from equipo e where id_equipo="+Equipo);
            while(rs.next()){
                datos.setNombre(rs.getString("nombre"));
                datos.setEstado(rs.getString("estado"));
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return datos;
    }    
    
    public objDestrezas cargarAtributosDestrezas(Integer Destreza){
        objDestrezas datos= new objDestrezas("","");
        try{
            connection=conexion.Conexion();
            s=connection.createStatement();
            rs= s.executeQuery("select perfil, fortaleza from destrezas");
            while(rs.next()){
                datos.setPerfil(rs.getString("perfil"));
                datos.setFortaleza(rs.getString("fortaleza"));
            }
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return datos;
    }
    
    public objJugadores cargarUnJugador(Integer Cedula){
        objJugadores datos=new objJugadores(-1,"",-1,"",-1,-1,"");
        try{
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs=  s.executeQuery("select j.cedula as cedula, j.nombre as nombre, j.edad as edad,j.estado as estado, j.equipo as equipo, j.destreza as destreza, j.fecha_debut as fecha_debut from jugadores j where j.cedula="+Cedula);
            while(rs.next()){
                datos.setCedula(rs.getInt("cedula"));
                datos.setNombre(rs.getString("nombre"));
                datos.setEdad(rs.getInt("edad"));
                datos.setEstado(rs.getString("estado"));
                datos.setEquipo(rs.getInt("equipo"));
                datos.setDestreza(rs.getInt("destreza"));
                datos.setFecha_debut(rs.getString("fecha_debut"));
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return datos;
    }
    
    public void modificarEquipo(objEquipos equipo, Integer idEquipo){
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("update equipo set "
                    + "nombre_equipo='"+equipo.getNombre()+"', "
                            + "estado="+equipo.getEstado()+" "
                                    + "where id_equipo="+idEquipo+"");
            if (z == 1) {
                JOptionPane.showMessageDialog(null, "Se modifico el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Error al modificar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void modificarUnJugador(objJugadores jugador, Integer cedula){
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z= s.executeUpdate("update jugadores set "
                    + "nombre='"+jugador.getNombre()+"', "
                            + "cedula="+jugador.getCedula()+", "
                                    + "edad="+jugador.getEdad()+", "
                                            + "equipo="+jugador.getEquipo()+", "
                                                    + "estado='"+jugador.getEstado()+"', "
                                                            + "destreza="+jugador.getDestreza()+", "
                                                                    + "fecha_debut='"+jugador.getFecha_debut()+"' "
                                                                            + "where cedula="+cedula);
            if (z==1){
                JOptionPane.showMessageDialog(null, "Se modifico el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Error al modificar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void ElimacionDeEquipo(Integer Equipo){
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("update jugadores set equipo=0 where equipo="+Equipo);
            int zl= s.executeUpdate("delete from equipo where id_equipo="+Equipo);
            if (zl==1) {
                JOptionPane.showMessageDialog(null, "Se Elimino el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Error al Eliminar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void EliminacionDeJugador(Integer Cedula){
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z= s.executeUpdate("delete from jugadores where cedula="+Cedula);
            if (z == 1) {
                JOptionPane.showMessageDialog(null, "Se Elimino el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showConfirmDialog(null, "Error al Eliminar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ArrayList<objRegistros> cargarRegistrosActivosConFortalezas(String Fortaleza) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.cedula as cedula, j.nombre as nombre,e.nombre_equipo as nombre_equipo,d.perfil as perfil from jugadores j, equipo e, destrezas d where j.equipo=e.id_equipo and j.destreza=d.id_destreza and j.estado=true and d.fortaleza='"+Fortaleza+"'");
            while (rs.next()) {
                objRegistros.listaRegistros.add(new objRegistros(
                        rs.getInt("cedula"), rs.getString("nombre"),
                        -1, "",
                        -1, -1,
                        "", -1,
                        rs.getString("nombre_equipo"), "",
                        -1, rs.getString("perfil"),
                        ""));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "11Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return objRegistros.listaRegistros;
    }

    public void insertarJugador(objJugadores jugador) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO JUGADORES values(" + jugador.getCedula() + ",'" + jugador.getNombre() + "'," + jugador.getEdad() + ",'" + jugador.getEstado() + "'," + jugador.getEquipo() + "," + jugador.getDestreza() + ",'" + jugador.getFecha_debut() + "')");
            if (z == 1) {
                JOptionPane.showMessageDialog(null, "Se agregó el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al insertar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertarEquipo(objEquipos equipo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO equipo values(" + cargarUltimoIDEquipo() + ",'" + equipo.getNombre() + "'," + equipo.getEstado() + ")");
            if (z == 1) {
                JOptionPane.showMessageDialog(null, "Se agregó el registro de manera éxitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al insertar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
}
