/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Lukestas
 */
public class BDConexion {
    private Connection connection=null;
    public Connection Conexion(){
        String url = "jdbc:postgresql://localhost:5432/Futbol";
        String password = "admin";
        try{
            Class.forName("org.postgresql.Driver");
            connection=DriverManager.getConnection(url,"postgres",password);
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Problema conectándose a la base de datos","Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        return connection;
    }
}
